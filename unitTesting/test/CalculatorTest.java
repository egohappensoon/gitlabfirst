import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

//import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.*;


public class CalculatorTest {
    @Rule
    public ExpectedException expectedException = ExpectedException.none(); // This is another way of catching exceptions

    @Test       //anotation
    public void sum() {
        //given
        Calculator calculator = new Calculator();
        //when
        int result = calculator.sum(4,7);
        //then
        assert result == 11;
    }
    @Test
    public void sumAssertEquals() {
        //given
        Calculator calculator = new Calculator();
        //when
        int result = calculator.sum(4,7);
        //then
       // assertEquals(11, result);
    }

    @Test
    public void subtract() {
        //given
        Calculator calculator = new Calculator();
        //when
        int result = calculator.subtract(4,7);
        //then
        assert result == -3;
    }

    @Test
    public void multiply() {
        //given
        Calculator calculator = new Calculator();
        //when
        int result = calculator.multiply(4,7);
        //then
        assert result == 28;
    }

    @Test
    public void division() {
        //given
        Calculator calculator = new Calculator();
        //when
        int result = calculator.division(4,2);
        //then
        assert result == 2;
    }

    @Test(expected = ArithmeticException.class) //catch exceptions
    public void divisionByZero() {
        //given
        Calculator calculator = new Calculator();
        //when
        double result = calculator.divisionByZero(4,0);
        //then
        //assert result == 0;
        //should throw an exception
    }
    @Test
    public void divisionWithByZero() {
        //given
        Calculator calculator = new Calculator();
        expectedException.expect(ArithmeticException.class);
        //when
        double result = calculator.divisionByZero(4,0);
        //then
        //should throw an exception
    }


    @Test
    public void testAssertJ(){
        //assertThat();
       // assertThat("Some text").isEqualTo("Something");
        assertThat("Some text").contains("text");

    }
}