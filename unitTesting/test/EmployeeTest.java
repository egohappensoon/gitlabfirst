import org.junit.Test;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.*;



public class EmployeeTest {

    @Test
    public void annualSalary() {
        // given
        Employee employee = new Employee("James","Dean",1000,true,false);
        // when
        double salary = employee.annualSalary();
        // then
        assertThat(salary).isEqualTo(12000);
        //assertThat(employee.annualSalary()).isEqualTo(12000);
    }

    @Test
    public void fullName() {
        // given
        Employee employee = new Employee("James","Dean",1000,true,false);
        // when
         String fullNames = employee.fullName();
        // then
        assertThat(fullNames).isEqualTo("JamesDean");
    }

    @Test
    public void numberOfVacationDays() {
        // given
        Employee employee = new Employee("James","Dean",1000,true,false);
        // when
        double salary = employee.annualSalary();
        // then

        ///default, if parent, if student
    }
}