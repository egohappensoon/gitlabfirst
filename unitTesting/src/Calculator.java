

public class Calculator {

    public int sum(int argument1, int argument2){
        return argument1 + argument2;
    }
    public int sumAssertEquals(int argument1, int argument2){
        return argument1 + argument2;
    }

    public int subtract(int argument1, int argument2){
        return argument1 - argument2;
    }

    public int multiply(int argument1, int argument2){
        return argument1 * argument2;
    }

    public int division(int argument1, int argument2){
        return argument1 / argument2;
    }

    public double divisionByZero(int argument1, int argument2){
        if (argument2 == 0){
            throw new ArithmeticException("Second argument can't be zero");
        }
        /*if (argument2 == 0){
            throw new ArithmeticException("You can't divide by zero");
        }*/
        return argument1 * 1.0 / argument2;
    }

    //Calculator.returnSomething();  -- static method, no need to create object

    public static int returnSomething(){
        return 0;
    }


}
