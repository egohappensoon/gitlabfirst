public class Employee {

    String firstName;
    String lastName;
    double monthlySalary;
    boolean isStudent;
    boolean isParent;


    //Methods
    // double annualSalary();
    //String fullName();
    //int numberOfVacationDays();  if parent +5days, if student +5days
    //Use AssertJ

    public Employee(String firstName, String lastName, double monthlySalary, boolean isStudent, boolean isParent) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.monthlySalary = monthlySalary;
        this.isStudent = isStudent;
        this.isParent = isParent;
    }

    public double annualSalary(){
        double annualSalary = monthlySalary * 12;
        System.out.println(annualSalary);
        return annualSalary;
    }

    public String fullName(){
        String fullName = firstName + lastName;
        System.out.println("My full names are: " + fullName);
        return fullName;
    }

    public int numberOfVacationDays(){

        int annualVacationDays = 28;

        if (isParent){
            int numberOfAnnualVacationDays = annualVacationDays + 5;
            System.out.println(numberOfAnnualVacationDays);
        }else if(isStudent){
            int numberOfAnnualVacationDays = annualVacationDays + 20;
            System.out.println(numberOfAnnualVacationDays);
        }else{
            System.out.println("No vacation for you!");
        }
        return annualVacationDays;
    }

    public String getFirstName() {
        return firstName;
    }

    public String setFirstName(String firstName) {
        this.firstName = firstName;
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String setLastName(String lastName) {
        this.lastName = lastName;
        return lastName;
    }

    public double getMonthlySalary() {
        return monthlySalary;
    }

    public double setMonthlySalary(double monthlySalary) {
        this.monthlySalary = monthlySalary;
        return monthlySalary;
    }

    public boolean isStudent() {
        return isStudent;
    }

    public void setStudent(boolean student) {
        isStudent = student;
    }

    public boolean isParent() {
        return isParent;
    }

    public void setParent(boolean parent) {
        isParent = parent;
    }


    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", monthlySalary=" + monthlySalary +
                ", isStudent=" + isStudent +
                ", isParent=" + isParent +
                '}';
    }
}
