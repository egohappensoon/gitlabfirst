package com.sda;

public class SumOf2Diagonals {

    // Find difference between sums of two diagonals
    public static void main(String[] args) {
        int n = 5;
        int[][] arr = {
                        {11, 2, 4, 3, 5},
                        {4, 5, 6, 8, 7},
                        {10, 8, -12, 6, 2},
                        {11, 9, 18, 16, -2},
                        {21, 22, 8, 3, 14}
                };
        System.out.print(findDiagonalDifferenceOfMatrix(arr, n));
    }

    private static int findDiagonalDifferenceOfMatrix(int[][] arr, int n) {

        int d1 = 0, d2 = 0;  // Initialize the sums of diagonals

        for (int i = 0; i < n; i++) {
            d1 += arr[i][i];
            d2 += arr[i][n - i - 1];
        }
        return Math.abs(d1 - d2);  // Absolute difference of the sum across the diagonals
    }



}
