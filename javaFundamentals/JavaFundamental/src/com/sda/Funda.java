
package com.sda;

import jdk.swing.interop.DropTargetContextWrapper;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Funda<row> {

    public static void main(String[] args) {

        //System.out.print("Hello\nHello");

        //printHello();
        // System.out.println();
        //round2DecimalPlaces();
        //align2Right();
        //displayCastedDouble();
        //sumOfMaxIntVariables();
        //floatByteChar();
        //greaterEqualLower();
        //twoValuesGreaterEqualLower();
        //oneValueGreaterEqualLower();
        //enteredNumbersName();
        //nestedLoopDrawTriangle();
        //nestedLoopWriteNumbers();
        //nestedLoopReversedNumbers();
        nestedLoopNumbersFlippedRight();
        //nestedLoopDrawRectangle();
        //nestedLoopDrawXmasTree();
        //nestedLoopDraw2ndXmasTree();
        //ifElseFlowMilkWine();
        //userDivideBy();
        //echoApplication();
        //sumOfIntergerArrays();


    }


    private static void printHello() {
        System.out.print("Hello\nHello");
    }

    private static void round2DecimalPlaces() {
        //Enter any value with several digits after the decimal point and assign it to variable of
        // type double. Display the given value rounded to two decimal places.
        double value = 23.46724;
        System.out.printf("Value: %.2f", value); // %.2f -- the amount of decimal places
    }

    private static void align2Right() {
        //Display any three strings of characters on one line so that they are aligned
        // to the right edge of the 15-character blocks. How to align strings to the left edge?

        String format1 = "%-15s %-15s %n";
        //String format2= "%15s %15s %n";
        String words = String.format(format1, "John", "Cash");
        System.out.println(words);
    }

    private static void displayCastedDouble() {
        //Enter two values of type int. Display their division casted to the double
        // type and rounded to the third decimal point.
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter a value: ");
        int input = scanner.nextInt();
        System.out.println("Please enter a value: ");
        int input2 = scanner.nextInt();
        System.out.println("Please enter another value: ");

        double division = (double) input / input2;
        System.out.printf("Casted/rounded value is: %.3f", division);
    }

    public static void sumOfMaxIntVariables() {
        //*Sum two integer variables initialized with maximal values for that type.
        //int firstInt = 2147483647;
        //int secondInt = 2147483647;
        int firstInt = Integer.MAX_VALUE;
        int secondInt = Integer.MAX_VALUE;
        System.out.println((long) firstInt + secondInt);
        //long sumOfMaxIntV = (long) firstInt + secondInt;
        //System.out.println(sumOfMaxIntV);
    }

    private static void floatByteChar() {
        //Create three variables, one for each type: float, byte and char. Enter values corresponding to
        //those types using Scanner. What values are you able to enter for each type?

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter value for float: ");
        float var1 = scanner.nextFloat();
        System.out.println("Enter value for byte: ");
        byte var2 = scanner.nextByte();
        System.out.println("Enter value for char: ");
        char var3 = scanner.next().charAt(0);

        System.out.println("Float is : " + var1 + "Byte is : " + var2 + ", Char is : " + var3);
    }

    private static void greaterEqualLower() {
        //Write an application that will show if entered value is greater, equal or lower than 30.
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter a value: ");
            int input = scanner.nextInt();
            if (input > 30) {
                System.out.println(input + " is greater than 30");
            } else if (input == 30) {
                System.out.println(input + " is equal to 30");
            } else {
                System.out.println(input + " is lower than 30");
            }
        } catch (InputMismatchException e) {
            //e.printStackTrace(); -- used here for debugging
            System.out.println("Entered value is not a number... ");
        }
    }

    private static void twoValuesGreaterEqualLower() {
        //As above but compare two values at the same time. Verify if first value is greater than 30 and
        //second value is greater than 30, and so on.
        long startTime = System.nanoTime();
        try {
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter first value: ");
            int input = scanner.nextInt();
            System.out.println("Enter second value: ");
            int input2 = scanner.nextInt();
            if (input > 30 && input2 > 30) {
                System.out.println(input + " and " + input2 + " are greater than 30");
            } else if (input == 30 && input2 == 30) {
                System.out.println(input + " and " + input2 + " are equal to 30");
            } else if (input < 30 && input2 < 30) {
                System.out.println(input + " and " + input2 + " are lower than 30");
            } else {
                System.out.println("One of these values are compatible!");
            }
        } catch (InputMismatchException e) {
            System.out.println("Entered number(s) is not allowed");
        }
        System.out.println(System.nanoTime() - startTime);
    }

    private static void oneValueGreaterEqualLower() {
        //As above but only one of the values has to be greater, equal or lower than 30.
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter first value: ");
        int inputValue = scanner.nextInt();
        System.out.println("Enter second value: ");
        int inputValue2 = scanner.nextInt();
        if (inputValue > 30 || inputValue2 > 30) {
            System.out.println(inputValue + " or " + inputValue2 + " are greater than 30");
        } else if (inputValue == 30 || inputValue2 == 30) {
            System.out.println(inputValue + " or " + inputValue2 + " are equal to 30");
        } else {
            System.out.println(inputValue + " or " + inputValue2 + " are lower than 30");
        }
    }


    private static void enteredNumbersName() {
        //Write an application that for any entered number between 0 and 9 will provide it’s name. For
        //example for “3” program should print “three”.
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter a value: ");
        int input = scanner.nextInt();
        switch (input) {
            case 0:
                System.out.println("Zero");
                //0 = Integer.parseInt("Zero");
                break;
            case 1:
                System.out.println("One");
                break;
            case 2:
                System.out.println("Two");
                break;
            case 3:
                System.out.println("Three");
                break;
            case 4:
                System.out.println("Four");
                break;
            case 5:
                System.out.println("Five");
                break;
            case 6:
                System.out.println("Six");
                break;
            case 7:
                System.out.println("Seven");
                break;
            case 8:
                System.out.println("Eight");
                break;
            case 9:
                System.out.println("Nine");
                break;
            default:
                System.out.println("No such number in range!");
        }
    }

    private static void enteredNumbersName2() {
        //Write an application that for any entered number between 0 and 9 will provide it’s name. For
        //example for “3” program should print “three”.
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter a value: ");
        int input = scanner.nextInt();
        /*if (){

        }*/
    }

    /**
     * Using nested for loops draw (parents loop iterator should be called “row”,
     * child – “column”):
     * a. triangle,
     * b. *rectangle with diagonals,
     * c. **Christmas tree
     **/
    private static void nestedLoopDrawTriangle() {
        //a. triangle,

        /*for (int row = 0; row < 5; row++) {
            System.out.println();
            System.out.println("      * * * * * * * * *".substring(row, 5 + 2 * row));
        }*/
        System.out.println();

        for (int row = 1; row <= 10; row++) {
            for (int column = 0; column < 10 - row; column++)
                System.out.print(" ");
            for (int columns = 0; columns < (2 * row - 1); columns++)
                System.out.print("*");
            System.out.println();
        }
    }

    private static void nestedLoopWriteNumbers() {
        //Numbers in ascending order
        int row = 10;
        for (int e = 0; e < row; e++) {
            for (int g = 0; g <= e; g++) {
                System.out.print(g);
            }
            System.out.println();
        }
    }
    private static void nestedLoopReversedNumbers() {
        //Numbers in descending order
        int row = 10;
        for (int d = 0; d <= row; d++) {
            for (int f = 0; f <= (row-d-1); f++) {
                System.out.print(f);
            }
            System.out.println();
        }
    }
    private static void nestedLoopNumbersFlippedRight() {
        //Numbers in ascending order flipped to the right
        int row = 10;
        for (int d = 0; d <= row; d++) {
            for (int f = 0; f <= (row-d-1); f++) {
                System.out.print(f);
            }
            System.out.println();
        }
    }

    private static void nestedLoopDrawXmasTree() {
        //c. **Christmas tree
        /*for (int row = 0; row < 5; row++) {
            System.out.println();
            System.out.println("      * * * * * * * * *".substring(row, 5 + 2 * row));
        }*/
        System.out.println();

        for (int row = 1; row <= 10; row++) {
            for (int column = 0; column < 10 - row; column++)
                System.out.print(" ");
            for (int columns = 0; columns < (2 * row - 1); columns++)
                System.out.print("*");
            System.out.println();
        }
        for (int row = 1; row <= 4 / 2; row++) {
            System.out.print(" ");
        }
        System.out.print("      ***   \n");
        for (int column = 0; column <= 4 / 2 - 3; column++) {
            System.out.print(" ");
        }
        System.out.print("      *******    \n");
    }

    private static void nestedLoopDraw2ndXmasTree() {
        //c. **Christmas tree
        for (int a = 0; a < 7; a++) {
            System.out.println();
            System.out.println("      * * * * * * * * *".substring(a, 5 + 2 * a));
        }
    }


    //Write a simple application that will simulate a shopping. Use only if-else flow control.
    //Consider following cases:
    //a. If you would like to buy a bottle of milk – cashier will ask you for a specific amount of
    //money. You have to enter that value and verify if it is same as the cashier asked.
    //b. If you would like to buy a bottle of wine – cashier will ask you if you are an adult and
    //for positive answer ask for a specific amount of money.
    private static void ifElseFlowMilkWine() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("What would you like, Milk or Wine? ");
        String optionEntered = scanner.nextLine();
        if (optionEntered.equalsIgnoreCase("Milk")) {
            System.out.println("That would cost you 15 euros.");
            System.out.println("Your money please: ");
            Scanner money = new Scanner(System.in);
            int milkMoney = money.nextInt();
            if (milkMoney == 15) {
                System.out.println("Thank you for giving the exact amount of money!");
            }
            else {
                System.out.println("Your money please: ");
                int milkMoneyAgain = money.nextInt();
                //System.out.println("Please, give the right amount of money!");
                if (milkMoneyAgain != 15) {
                    System.out.println("Sorry, you don't have the right amount of money!");
                }
                ifElseFlowMilkWine();
            }
        } else if (optionEntered.equalsIgnoreCase("Wine")) {
            System.out.println("Are you an adult?");
            String adultResponse = scanner.nextLine();
            if (adultResponse.equalsIgnoreCase("Yes")) {
                System.out.println("That would cost you 25 euros. ");
            } else {
                System.out.println("You are a teenager, please leave my shop!");
            }
        } else {
            System.out.println("Please buy something.");
        }
    }

    //Write a “divide by” application. User should be able to enter initial value that will be divided
    //in a loop by a new value entered by a user. Division should occur as long, as entered value will be different than 0.
    // Result of division should be rounded to the fourth decimal point and printed to the console
    private static void userDivideBy() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please enter initial value: ");
        long initialInput = scanner.nextLong();
        System.out.println("Please enter final value: ");
        while (true) {  //while(true) -- infinite loop
            long finalInput = scanner.nextLong();
            if (finalInput == 0) {
                System.out.println("This can't be divided by 0! ");
                break;
            } else {
                double result = (double) initialInput / finalInput;
                System.out.println(String.format("%.4f", result));
            }
        }
    }

    //Write a simple “echo” application, that will:
    //a. print back entered string,
    //b. go to the beginning of a loop if user will enter “continue”,
    //c. break the loop with a “good bye!” message, if user will enter “quit”.
    private static void echoApplication() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please, enter a word: ");
        String inputData = scan.nextLine();
        if (inputData.equalsIgnoreCase("Continue")) {
            System.out.println("You entered: " +"'"+inputData+"'" + "\n");
            echoApplication();
        }else if(inputData.equalsIgnoreCase("quit")){
            System.out.println(" . . .  Gooooooooooood bye!");
        }else {
            echoApplication();
        }
    }

    private static void sumOfIntergerArrays() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Please, enter numbers: ");
        int inputData = scanner.nextInt();
        int sum = 0;
        for (int i = 0; i < inputData; i++) {
            sum = sum + (scanner.nextInt());
        }
        System.out.println(sum);
    }

    //Write an application that will find biggest value within array of int variables.
    //a. check your application using randomly generated array (use Random class),
    //b. check your application at least 5 times in a loop (generate random array -> print
    //array to the console -> find biggest value -> print biggest value -> manually verify results).
    private static void findBiggestValueIn(){

    }


}

