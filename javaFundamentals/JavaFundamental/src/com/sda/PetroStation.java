package com.sda;

public class PetroStation {

    double volume;
    double price;

    public PetroStation(){ }

    public PetroStation(double volume, double price) {
        this.volume = volume;
        this.price = price;
    }

    public double getVolume() {
        return volume;
    }

    public void setVolume(double volume) {
        this.volume = volume;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "PetroStation{" +
                "volume=" + volume +
                ", price=" + price +
                '}';
    }
}
