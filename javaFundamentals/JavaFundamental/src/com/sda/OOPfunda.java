package com.sda;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class OOPfunda extends Products {

    public static void main(String[] args) {


        //groceryShop();
        petroStation();


    }

    public static void groceryShop() {
        //1. Grocery Shopping
        //a. Create class Product, it should contain at least two fields – name and price.
        //b. Create an empty array of Products – it’s size should be at least 5.
        //c. Fill it within while loop. Simulate the process of doing shopping:
        //i. ask for a product,
        //ii. add it to the cart (array),
        //iii. change index,
        //iv. if index will be greater than 5 – finish shopping,
        //v. *pay for the products.

        //Products product = new Products();
        /*product.setName("Apple");
        product.setPrice(1);
        System.out.println("Name is: " + product.getName());
        System.out.println("Price is: " + product.getPrice());*/

        System.out.println("Enter the products you would like?");
        Products product = new Products();
        Products[] products = new Products[5];
        Random random = new Random(10);

        for (int i = 0; i < 5; i++) {
            Scanner scanner = new Scanner(System.in);
            String productName = scanner.nextLine();
            double price = random.nextDouble();
            products[i] = new Products();
            products[i].setName(productName);
            products[i].setPrice(price);
        }
        System.out.println(Arrays.toString(products));
    }


    public static void petroStation() {
        //2. Petrol Station
        //a. Simulate the process of refueling. Within the while loop ask user if you should
        //continue or finish. For every entered “continue” command you should add a specific
        //amount of petrol and money (both of type double) and view it on the console.
        // b. At the end user should pay for petrol. Consider multiple possibilities, like:
        //i. The user paid exactly as much as required.
        //ii. The user paid too much (cashier should return the rest of the money).
        //iii. The user paid too little – should be asked for the rest.

        //PetroStation petroStation = new PetroStation();
        boolean goOn = true;
        Scanner scanner = new Scanner(System.in);
        Scanner volumes = new Scanner(System.in);
        double price = 1.5D;
        double total = 1;

        while (goOn) {
            System.out.println("Enter volume please: ");
            double volume = volumes.nextDouble();
            System.out.println("Please, enter 'Continue or Finish! ");
            String word = scanner.nextLine();

            if (word.equalsIgnoreCase("Continue")) {
                total = price * volume;
                System.out.println("Price is: " + price);
                System.out.println("Volume is: " + volume);
                System.out.println("Euro "+total);

            } else if (word.equalsIgnoreCase("Finish")) {
                System.out.println("Euro "+total);
                break;
            } else {
                System.out.println("Please enter the amount you need... \n");
            }
        }
    }


}
