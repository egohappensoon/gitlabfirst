package com.sda;

import java.util.Arrays;

public class TestingDay {

    public static void main(String[] args) {


        //dayOfTheProgrammer(1999);
        System.out.println(dayOfTheProgrammer(1977));
//        System.out.println("Sum from 1 to N: ");
//        System.out.println(sumFromOneToN(2));
//        System.out.println(sumFromOneToN(4));
//        System.out.println(sumFromOneToN(0));
//        System.out.println(sumFromOneToN(-5));
//        testSumFromOneToN(2);

        /** Setting values for sum of arrays as well as calling method sumOfArrays() **/
        int[] myArrays = {5,2,10,1,6,7,9,8 };
        System.out.println("The sum of arrays: "+ sumOfArrays(myArrays));


    }



    public static String dayOfTheProgrammer(int year) {

        String leapYear = "12.09.";
        String nonLeapYear = "13.09.";
        String result = "";
        if (year == 1918) {
            result = "26.09.1918";
        }
        if (year<1918) {
            if (year % 4 == 0) {
                result = leapYear + year;
            } else {
                result = nonLeapYear + year;
            }
        }
        if ( year > 1918) {
            if (year % 400 == 0||(year % 4 == 0 && year % 100 != 0)) {
                result = leapYear+year;
            } else {
                result = nonLeapYear + year;
            }
        }
        return result;
    }

    public static int sumFromOneToN(int n){
        int sum = 0;
        for(int i = 1; i <= n; i++){
            sum = sum + i;
        }
        return sum;
    }

    public static int sumOfArrays(int[] arr){
        int sumOfArrays = 0;
        for (int i = 0; i < arr.length; i++){
            sumOfArrays += arr[i];
        }
        return sumOfArrays;
    }

    public static void testSumFromOneToN(int n){
        int expectedResult = -5;    // n=2
        int actualResult = sumFromOneToN(2);
        if (expectedResult != actualResult){
            throw new ArithmeticException();
        }
    }



}
